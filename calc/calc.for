      PROGRAM calc
       INTEGER A, B, I, C, SIGN, OP
       INTEGER ZERO, NINE, PLUS, MINUS, MULT, DIV, POW
       CHARACTER INPUT*20
       PARAMETER (ZERO = ICHAR('0'), NINE  = ICHAR('9'),
     $            PLUS = ICHAR('+'), MINUS = ICHAR('-'),
     $            MULT = ICHAR('*'), DIV   = ICHAR('/'),
     $            POW  = ICHAR('^'))
       WRITE(*,*)'Enter expression (NUM{operator}NUM):'
       READ(*,'(A20)')INPUT
       A = 0
       B = 0
       I = 1
       OP = 0
       IF(ICHAR(INPUT(I:I)) .EQ. ICHAR('-')) THEN
        SIGN = -1
        I = I+1
       ELSE
        SIGN = 1
       END IF
  100   C = ICHAR(INPUT(I:I))
        IF(      ZERO .LE. C
     $     .AND. C    .LE. NINE) THEN
         A = A*10 + (C-ZERO)
        ELSE
         IF (     C .EQ. PLUS
     $       .OR. C .EQ. MINUS
     $       .OR. C .EQ. MULT
     $       .OR. C .EQ. DIV
     $       .OR. C .EQ. POW) THEN
          OP = C
          I = I+1
          GOTO 200
         END IF
        END IF
        I = I+1
        IF(I .LE. 20) THEN
         GOTO 100
        END IF
  200  A = A*SIGN
       IF(ICHAR(INPUT(I:I)) .EQ. ICHAR('-')) THEN
        SIGN = -1
        I = I+1
       ELSE
        SIGN = 1
       END IF
  300   C = ICHAR(INPUT(I:I))
        IF(      ZERO .LE. C
     $     .AND. C    .LE. NINE) THEN
         B = B*10 + (C-ZERO)
        END IF
        I = I+1
        IF(I .LE. 20) THEN
         GOTO 300
        END IF
  400  B = B*SIGN
       IF(OP .EQ. PLUS) THEN
        WRITE(*,'(I10)')(A+B)
       ELSE
        IF(OP .EQ. MINUS) THEN
         WRITE(*,'(I10)')(A-B)
        ELSE
         IF(OP .EQ. MULT) THEN
          WRITE(*,'(I10)')(A*B)
         ELSE
          IF(OP .EQ. DIV) THEN
           IF (B .EQ. 0) THEN
            STOP 'Division by zero is prohibited'
           END IF
           WRITE(*,'(F10.5)')(REAL(A)/B)
          ELSE
           IF(OP .EQ. POW) THEN
            WRITE(*,'(I10)')(A**B)
           END IF
          END IF
         END IF
        END IF
       END IF
      END
