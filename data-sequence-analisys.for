      PROGRAM DSA
        DOUBLE PRECISION VARS(ICHAR('Z')-ICHAR('A')+1)
        CHARACTER IN(128), OUT(128), C
        DOUBLE PRECISION XMIN, XMAX, XSTEP, VV, R
        INTEGER N, IL, OL, VI, XI, I
        DOUBLE PRECISION EFQ, EF, FMIN, FMAX, AVG
        INTEGER FN, FNP, FNN
        EFQ=0
        EF=0
        FN=0
        FNP=0
        FNN=0
        WRITE(*,*)'Input formula (without spaces):'
        CALL INSTR(IN, 128, IL)
        WRITE(*,*)'Input X range (start end):'
        CALL INDBL(XMIN)
        CALL INDBL(XMAX)
        WRITE(*,*)'Input number of iterations:'
        CALL ININT(N)
        WRITE(*,*)'Input variables (v value, use newline to interrupt):'
  100     READ(UNIT=*,FMT='(A,$)',ERR=200)C
          IF(C .EQ. ' ')THEN
            GO TO 200
          END IF
          VI=ICHAR(C)-ICHAR('a')+1
          READ(*,'(A,$)')C
          CALL INDBL(VV)
          VARS(VI)=VV
          GO TO 100
  200   CALL LEX(IN, OUT, IL, OL)
        CALL PARSE(OUT, OL)
        XI=ICHAR('X')-ICHAR('A')+1
        XSTEP=(XMAX-XMIN)/(N-1)
        I=0
  300     VARS(XI)=XMIN+I*XSTEP
          CALL EXECUTE(OUT, OL, VARS, R)
          CALL PROCF(R, EFQ, EF, FMIN, FMAX, FN, FNP, FNN)
          I=I+1
        IF(I .LT. N)THEN
          GO TO 300
        END IF
        AVG=EF/FN
        WRITE(*,*)'Minimal value: ', FMIN
        WRITE(*,*)'Maximal value: ', FMAX
        WRITE(*,*)'Average: ', AVG
        WRITE(*,*)'Average square: ', EFQ/FN
        WRITE(*,*)'Average square root: ', SQRT(EFQ/FN)
        WRITE(*,*)'Relative amount of positive numbers: ', DBLE(FNP)/FN
        WRITE(*,*)'Relative amount of negative numbers: ', DBLE(FNN)/FN
        WRITE(*,*)'Sigma: ', SQRT(AVG**2+EFQ/FN-2*AVG*EF/FN)
      END
      SUBROUTINE PROCF(R, EFQ, EF, FMIN, FMAX, FN, FNP, FNN)
        DOUBLE PRECISION R, EFQ, EF, FMIN, FMAX
        INTEGER FN, FNP, FNN
        IF(FN .EQ. 0)THEN
          FMIN=R
          FMAX=R
        ELSE
          IF(R .LT. FMIN)THEN
            FMIN=R
          END IF
          IF(R .GT. FMAX)THEN
            FMAX=R
          END IF
        END IF

        FN=FN+1
        IF(R .GT. 0)THEN
          FNP=FNP+1
        ELSE
          IF(R .LT. 0)THEN
            FNN=FNN+1
          END IF
        END IF

        EFQ=EFQ+R*R
        EF=EF+R
      END SUBROUTINE
c vim: ts=2 sts=2 sw=2 tw=72 ft=fortran fenc=utf-8
