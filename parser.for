c     PROGRAM PARSER
c       CHARACTER INPUT*128, OUTPUT*128
c       INTEGER LEVEL
c       INTEGER L, I
c       DO 25, I=1,128
c         INPUT(I:I)=' '
c  25   END DO
c       READ(*,'(A128)') INPUT
c       DO 30, I=1,128
c         IF(LEVEL(INPUT(I:I)) .EQ. 0)THEN
c           L=I-1
c           GO TO 35
c         END IF
c  30   END DO
c  35   CALL PARSE(INPUT, L)
c       WRITE(*,'(A(L))') INPUT
c     END PROGRAM
      SUBROUTINE PARSE(OUT, L)
        CHARACTER OUT*(*)
        INTEGER L
        INTEGER LEVEL
        INTEGER I, LEV
        CHARACTER C
        LEV=1
   50     I=L
   75       C=OUT(I:I)
            IF(LEVEL(C) .EQ. LEV)THEN
              CALL MOVE(OUT, L, I+1, LEV+1)
            END IF
            I=I-1
          IF(I .GT. 0)THEN
            GO TO 75
          END IF
          LEV=LEV+1
        IF(LEV .LT. 5)THEN
          GO TO 50
        END IF
        I=1
   80     C=OUT(I:I)
          IF(C .EQ. '(' .OR. C .EQ. ')')THEN
            CALL REMOVE(OUT, L, I)
          ELSE
            I=I+1
          END IF
        IF(I .LE. L)THEN
          GO TO 80
        END IF
      END SUBROUTINE
      SUBROUTINE REMOVE(OUT, L, I)
        CHARACTER OUT*(*)
        INTEGER L, I
        OUT(I:(L-1))=OUT((I+1):L)
        OUT(L:L)=' '
        L=L-1
      END SUBROUTINE
      SUBROUTINE MOVE(OUT, L, START, MLEVEL)
        CHARACTER OUT*(*)
        INTEGER L, START, MLEVEL
        INTEGER LEVEL
        INTEGER I, OLDPOS, NEWPOS, INL, PLEV, LEV
        CHARACTER C
        OLDPOS=START-1
        I=START
        INL=0
  100     C=OUT(I:I)
          IF(C .EQ. '(')THEN
            IF(     INL .GT. 0
     $         .OR. I .EQ. START
     $         .OR. PLEV .GE. MLEVEL)THEN
              INL=INL+1
              PLEV=5
              GO TO 200
            ELSE
              GO TO 300
            END IF
          ELSE
            IF(C .EQ. ')')THEN
              INL=INL-1
              IF(INL .LT. 0)THEN
                GO TO 300
              ELSE
                PLEV=5
                GO TO 200
              END IF
            ELSE
              IF(INL .GT. 0)THEN
                PLEV=5
                GO TO 200
              ELSE
                LEV=LEVEL(C)
                IF((     I .EQ. START
     $              .OR. (1 .LE. PLEV .AND. PLEV .LE. 3)
     $              .OR. (1 .LE.  LEV .AND.  LEV .LE. 3))
     $             .AND. LEV .GE. MLEVEL)THEN
                  PLEV=LEV
                  GO TO 200
                ELSE
                  GO TO 300
                END IF
              END IF
            END IF
          END IF
  200   I=I+1
        IF(I .LE. L)THEN
          GO TO 100
        END IF
  300   NEWPOS=I-1
        CALL SHIFT(OUT, OLDPOS, NEWPOS)
      END SUBROUTINE
      SUBROUTINE SHIFT(OUT, OLDPOS, NEWPOS)
        CHARACTER OUT*(*)
        INTEGER OLDPOS, NEWPOS
        INTEGER I
        CHARACTER C
        C=OUT(OLDPOS:OLDPOS)
        OUT(OLDPOS:(NEWPOS-1))=OUT((OLDPOS+1):NEWPOS)
        OUT(NEWPOS:NEWPOS)=C
      END SUBROUTINE
      INTEGER FUNCTION LEVEL(ATOM)
        CHARACTER ATOM
        INTEGER AI
        INCLUDE "constants.for"
        AI=ICHAR(ATOM)
        IF(     (ICHAR('0') .LE. AI .AND. AI .LE. ICHAR('9'))
     $     .OR. (ICHAR('A') .LE. AI .AND. AI .LE. ICHAR('Z'))
     $     .OR. (ATOM .EQ. C_PI))THEN
          LEVEL=5
        ELSE
          IF(     ATOM .EQ. '('
     $       .OR. ATOM .EQ. ')')THEN
            LEVEL=5
          ELSE
            IF(ICHAR('a') .LE. AI .AND. AI .LE. ICHAR('z'))THEN
              LEVEL=4
            ELSE
              IF(ATOM .EQ. '^')THEN
                LEVEL=3
              ELSE
                IF(     ATOM .EQ. '*'
     $             .OR. ATOM .EQ. '/')THEN
                  LEVEL=2
                ELSE
                  IF(     ATOM .EQ. '+'
     $               .OR. ATOM .EQ. '-'
     $               .OR. ATOM .EQ. '!')THEN
                    LEVEL=1
                  ELSE
                    LEVEL=0
                  END IF
                END IF
              END IF
            END IF
          END IF
        END IF
      END FUNCTION
c vim: ts=2 sts=2 sw=2 tw=72 ft=fortran fenc=utf-8
