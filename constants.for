      CHARACTER F_COS, F_SIN, F_TG,  F_ASIN, F_ACOS, F_ATG
      CHARACTER F_SH,  F_CH,  F_LN,  F_EXP
      CHARACTER C_PI
      PARAMETER (F_SIN  = 's', F_COS  = 'c', F_TG  = 't',
     $           F_ASIN = 'q', F_ACOS = 'b', F_ATG = 'u',
     $           F_SH   = 'h', F_CH   = 'i',
     $           F_EXP  = 'e', F_LN   = 'l',
     $           C_PI   = '#')
c vim: ts=2 sts=2 sw=2 tw=72 ft=fortran fenc=utf-8
